# IA04 TD PROJET

travail réalisé par :  
- CHAOUCHI Nasser
- ERRAZINE Mohamed Adnane

## Lancer le programme sous Linux

Créer un dossier et initialiser un module
```
go mod init monSuperModule
```
Récupérer le 'repository' de gitlab et Installer le programme
```
go get gitlab.utc.fr/merrazin/IA04_TD_PROJET
go install gitlab.utc.fr/merrazin/IA04_TD_PROJET/cmd/launch_auto
go install gitlab.utc.fr/merrazin/IA04_TD_PROJET/cmd/launch_server
```

## Données stockées dans le serveur.

### Bureau de vote `Ballot`

| Champ        | Type              | Balise JSON       | Description                                       |
|--------------|-------------------|------------------|---------------------------------------------------|
| Ballot_id    | string            | `json:"ballot-id"` | Identifiant du bureau de vote                          |
| Règle        | string            | `json:"rule"`     | La règle de vote |
| Date limite  | string            | `json:"deadline"` | Date limite pour soumettre les votes          |
| Voter_ids    | map[string]bool   | `json:"voter-ids"`| Carte des identifiants des votants pour vérifier si un votant a déjà voté |
| Alternatives | int               | `json:"alts"`    | Nombre d'alternatives            |
| Tie-break    | []int             | `json:"tie-break"` | Liste d'entiers utilisés pour départager en cas d'égalité |

À noter que `Voter_ids` est de type map, car nous souhaitons conserver les informations sur les votants qui ont voté.
 
### Electeur `Voter`
| Champ     | Type                                | Balise JSON        | Description                        |
|-----------|-------------------------------------|-------------------|------------------------------------|
| Ballot_id | string                              | `json:"ballot-id"`  | Identifiant du bulletin            |
| Prefs     | []int                               | `json:"prefs"`      | Préférences de l'électeur          |
| Options   | []int                               | `json:"options"`    | Optionnel, nous pourrions ajouter un pointeur plus tard |


À noter que dans la structure de données `Voter`, nous ne conservons pas l'identifiant de l'électeur pour préserver l'anonymat des préférences des électeurs.

## Tests automatiques

Dans cette section, nous exécutons 20 `Ballots` ou bureaux de vote. Les règles et le nombre d'alternatives sont choisis au hasard, tout comme les dates limites, qui sont sélectionnées parmi 3 dates futures. Il existe 3 types d'agents :
- RestClientBallotCreatorAgent
- RestClientVoterAgent
- RestClientResultAgent

Ils sont tous créés avant toute exécution. Ensuite, nous lançons des goroutines qui exécutent en concurrence les bureaux de vote, et chaque bureau de vote fait exécuter tous ses agents électeurs. Enfin, nous exécutons les RestClientResultAgent pour afficher les résultats de chaque bureau de vote.

## Exemple (Dossier serverPictures)
Nous avons effectué plusieurs exemples en envoyant les requêtes via le plugin fourni. Dans le dossier "serverPictures", nous avons pris des captures d'écran de différentes règles de vote, ainsi que quelques opérations de base (Voter), ainsi que des démonstrations de la réponse du serveur lorsque les requêtes ne sont pas valides, notamment lorsqu'un électeur tente de voter deux fois.
