package main

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	ca "gitlab.utc.fr/merrazin/IA04_TD_PROJET/clientAgent"
	sa "gitlab.utc.fr/merrazin/IA04_TD_PROJET/serverAgent"
)

func create_RandomVectorWithoutReplacement(Alts int) []int {
	var tie_break []int
	for i := 0; i < Alts; i++ {
		tie_break = append(tie_break, i)
	}
	rand.Shuffle(len(tie_break), func(i, j int) { tie_break[i], tie_break[j] = tie_break[j], tie_break[i] })
	return tie_break
}

func main() {
	currentTime := time.Now()
	seed := currentTime.Unix()
	rand.Seed(seed)
	const n_ballots = 20
	const url1 = ":8010"
	const url2 = "http://localhost:8010"
	servAgt := sa.NewRestServerAgent(url1)
	ballotAgents := make([]ca.RestClientBallotCreatorAgent, 0, n_ballots)
	voterAgents := make([]ca.RestClientVoterAgent, 0)
	resultAgents := make([]ca.RestClientResultAgent, 0, n_ballots)
	compteurVoteur := 0

	log.Println("démarrage du serveur...")
	go servAgt.Start()

	// create ballots
	rules := []string{"majority", "borda", "condorcet", "copeland", "approval", "stv"}
	deadlines := []string{"2025-10-09T23:05:08+02:00", "2026-10-09T23:05:08+02:00", "2025-11-09T23:05:08+02:00"}

	for i := 0; i < n_ballots; i++ {
		Alts := rand.Intn(20-2+1) + 2 // max 20 alternatives
		rule := rules[rand.Intn(len(rules))]
		deadline := deadlines[rand.Intn(len(deadlines))]
		tie_break := create_RandomVectorWithoutReplacement(Alts)
		var voter_ids []string
		participants := compteurVoteur + rand.Intn(20-2+1) + 2
		for i := compteurVoteur; i < participants; i++ { // max 20 voters
			voter_ids = append(voter_ids, fmt.Sprintf("ag_id%d", i))
			compteurVoteur++
		}
		//Ballot := ras.New_ballot{Rule: rule, Deadline: deadline, Voter_ids: voter_ids, Alts: Alts, Tie_break: tie_break}
		bltAgent := ca.NewRestClientBallotCreatorAgent(rule, deadline, voter_ids, Alts, tie_break, url2)
		ballotAgents = append(ballotAgents, *bltAgent)
	}
	// for each ballot, create voters
	for _, bltAgent_i := range ballotAgents {
		for _, voter_id := range bltAgent_i.Voter_ids {
			prefs := create_RandomVectorWithoutReplacement(bltAgent_i.Alts)
			options := []int{}
			if bltAgent_i.Rule == "approval" {
				randomValue := rand.Intn(bltAgent_i.Alts + 1)
				options = append(options, randomValue)
			}

			vtrAgent := ca.NewRestClientVoterAgent(voter_id, bltAgent_i.Ballot_id, prefs, options, url2)
			voterAgents = append(voterAgents, *vtrAgent)
		}
	}

	for _, bltAgent_i := range ballotAgents {
		rstAgent := ca.NewRestClientResultAgent(bltAgent_i.Ballot_id, url2)
		resultAgents = append(resultAgents, *rstAgent)
	}

	var wg sync.WaitGroup

	for i, bltAgent_i := range ballotAgents {
		wg.Add(1)
		go func(i int, bltAgent_i ca.RestClientBallotCreatorAgent) {
			defer wg.Done()
			bltAgent_i.Start(&voterAgents, &resultAgents[i])
		}(i, bltAgent_i)
	}

	wg.Wait()

}
