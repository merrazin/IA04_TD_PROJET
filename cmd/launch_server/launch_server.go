package main

import (
	"fmt"

	sa "gitlab.utc.fr/merrazin/IA04_TD_PROJET/serverAgent"
)

func main() {
	server := sa.NewRestServerAgent(":8010")
	server.Start()
	fmt.Scanln()
}
