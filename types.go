package td5AdnaneModule

type Request struct {
	Operator string `json:"op"`
	Args     [2]int `json:"args"`
}

type Response struct {
	Result int `json:"res"`
}

type Ballot struct {
	Ballot_id string          `json:"ballot-id"`
	Rule      string          `json:"rule"`
	Deadline  string          `json:"deadline"`
	Voter_ids map[string]bool `json:"voter-ids"` // map of voter ids to check if a voter has already voted
	Alts      int             `json:"#alts"`
	Tie_break []int           `json:"tie-break"`
}
type Voter struct { // no need for voter_id because we want votes to be annonymous in our Database
	Ballot_id string `json:"ballot-id"`
	Prefs     []int  `json:"prefs"`
	Options   []int  `json:"options"` //optional, Might add a pointer later
}
type New_ballot struct {
	Rule      string   `json:"rule"`
	Deadline  string   `json:"deadline"`
	Voter_ids []string `json:"voter-ids"`
	Alts      int      `json:"#alts"`
	Tie_break []int    `json:"tie-break"`
}
type Response_New_ballot struct {
	Ballot_id string `json:"ballot-id"` // si code 201
}
type Vote struct {
	Agent_id  string `json:"agent-id"`
	Ballot_id string `json:"ballot-id"`
	Prefs     []int  `json:"prefs"`
	Options   []int  `json:"options"` //optional, Might add a pointer later
}
type Result struct {
	Ballot_id string `json:"ballot-id"`
}
type Response_Result struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}
