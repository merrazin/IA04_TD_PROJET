package td5AdnaneModule

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

func rank(alt Alternative, prefs []Alternative) int {
	for ind, al := range prefs {
		if al == alt {
			return ind
		}
	}
	return -1
}

func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return rank(alt1, prefs) < rank(alt2, prefs)
}

func maxCount(count Count) (bestAlts []Alternative) {
	var max int = math.MinInt64
	for _, val := range count {
		if val > max {
			max = val
		}
	}
	for key, val := range count {
		if val == max {
			bestAlts = append(bestAlts, key)
		}
	}
	return bestAlts
}

func checkProfile(prefs []Alternative, alts []Alternative) error {
	if len(prefs) != len(alts) {
		return fmt.Errorf("len(prefs) != len(alts) // func checkProfile ")
	}
	var sorted_prefs []int
	var sorted_alts []int
	for _, val := range alts {
		sorted_alts = append(sorted_alts, int(val))
	}
	for _, val := range prefs {
		sorted_prefs = append(sorted_prefs, int(val))
	}
	sort.Ints(sorted_alts)
	sort.Ints(sorted_prefs)
	for i := 0; i < len(alts); i++ {
		if sorted_prefs[i] != sorted_alts[i] {
			return fmt.Errorf("sorted_prefs[i] != sorted_alts[i] // func checkProfile")
		}
	}
	return nil
}

func checkProfile_pArg(p Profile) error {

	if len(p) == 0 {
		return fmt.Errorf("profil vide")
	}
	for _, prefs := range p {
		if len(prefs) != len(p[0]) {
			return fmt.Errorf("profil n'est pas complet")
		}
	}

	for _, prefs := range p {
		dict_uniqueness := make(map[Alternative]int)
		for _, pref := range prefs {
			dict_uniqueness[pref] += 1
			if dict_uniqueness[pref] > 1 {
				return fmt.Errorf("profil n'est pas unique")
			}
		}
	}
	return nil
}

func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for ind, _ := range prefs {
		r := checkProfile(prefs[ind], alts)
		if r != nil {
			return r
		}
	}
	return nil
}

func ConvertProfileIntToAlts(prefs [][]int) Profile {
	var profile Profile
	for _, prefs := range prefs {
		var prefs_alternative []Alternative
		for _, pref := range prefs {
			prefs_alternative = append(prefs_alternative, Alternative(pref))
		}
		profile = append(profile, prefs_alternative)
	}
	return profile
}

func ConvertProfileAltsToInt(prefs Profile) [][]int {
	var profile_int [][]int
	for _, prefs := range prefs {
		var prefs_int []int
		for _, pref := range prefs {
			prefs_int = append(prefs_int, int(pref))
		}
		profile_int = append(profile_int, prefs_int)
	}
	return profile_int
}
func ConvertAltsToInt(alts []Alternative) []int {
	var alts_int []int
	for _, alt := range alts {
		alts_int = append(alts_int, int(alt))
	}
	return alts_int
}

func ConvertIntToAlts(alts_int []int) []Alternative {
	var alts []Alternative
	for _, alt := range alts_int {
		alts = append(alts, Alternative(alt))
	}
	return alts
}

//func SWFFactory(func swf(p Profile) (Count, error), func ([]Alternative) (Alternative, error)) (func(Profile) ([]Alternative, error))
//	fonction SWF classique, fonction de tie break

func MajoritySWF(p Profile) (count Count, err error) {
	if checkProfile_pArg(p) != nil {
		return nil, fmt.Errorf("ERROR MajoritySWF // checkProfile_pArg")
	}
	count = make(map[Alternative]int)
	for _, alt := range p[0] {
		count[alt] = 0
	}
	for _, list := range p {
		count[list[0]] += 1
	}
	return count, nil
}
func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), nil
}

// //////////////////////////////////////////////////////////////////////////////////////
// //////////////////////////////////////////////////////////////////////////////////////
// à vérifier
func BordaSWF(p Profile) (count Count, err error) {
	count = make(map[Alternative]int)
	if len(p) == 0 {
		return nil, fmt.Errorf("ERROR bordaSWF")
	}

	for _, i := range p {
		coef := len(p[0])
		for _, j := range i {
			coef--
			count[j] += coef
		}
	}
	return count, nil
}
func BordaSCF(p Profile) (bestAlts []Alternative, err error) {

	cout_ini, err := BordaSWF(p)
	if err != nil {
		return nil, fmt.Errorf("ERROR BORDASCF")
	}
	return maxCount(cout_ini), nil
}

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("ERROR APPROVALSWF")
	}
	if len(p) != len(thresholds) {
		return nil, fmt.Errorf("ERROR APPROVALSWF_threshold")
	}
	count = make(map[Alternative]int)
	for ind, i := range p {
		for _, j := range i[:thresholds[ind]] {
			count[j] += 1
		}
	}
	return count, nil
}
func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, fmt.Errorf("ERROR ApprovalSCF")
	}
	return maxCount(count), nil
}

var tiebreak_general func([]Alternative) (Alternative, error)

func TieBreakFactory(orderedAlt []Alternative) func([]Alternative) (Alternative, error) {

	return func(alts []Alternative) (Alternative, error) {
		var index []int
		for _, elem := range alts {
			for i, elem2 := range orderedAlt {
				if elem2 == elem {
					index = append(index, i)
				}
			}
		}
		if len(index) == 0 {
			randint := rand.Intn(len(alts))
			return alts[randint], nil
		}
		if len(index) == 1 {
			return orderedAlt[index[0]], nil
		}
		min := index[0] // Initialisez min avec la première valeur du slice

		for _, value := range index {
			if value < min {
				min = value // Mettez à jour la valeur minimale si une valeur plus petite est trouvée
			}
		}

		return orderedAlt[min], nil
	}

}

func SWFFactory(swf func(Profile) (Count, error), tieBreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) ([]Alternative, error) {
		list_losers = []Alternative{}
		tiebreak_general = tieBreak
		swfResult, err := swf(p)
		if err != nil {
			return nil, err
		}
		var sl []Alternative

		size := len(swfResult)
		for i := 0; i < size; i++ {
			bestAlts := maxCount(swfResult)
			if len(bestAlts) == 1 {
				sl = append(sl, bestAlts[0])
				delete(swfResult, bestAlts[0])
			} else {
				alt, err := tieBreak(bestAlts)
				if err != nil {
					return nil, err
				}
				sl = append(sl, alt)
				delete(swfResult, alt)

			}
		}
		if len(list_losers) != 0 {
			size = int(len(list_losers))
			for i := size - 1; i > -1; i-- {
				sl = append(sl, list_losers[i])
			}

		}
		return sl, nil
	}
}

func SCFFactory(scf func(Profile) ([]Alternative, error), tieBreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		tiebreak_general = tieBreak
		scfResult, err := scf(p)
		if err != nil {
			return -1, err
		}
		if scfResult == nil {
			return Alternative(-1), nil
		}
		if len(scfResult) == 1 {
			return scfResult[0], nil
		} else {
			alt, err := tieBreak(scfResult)
			if err != nil {
				return -1, err
			}
			return alt, nil
		}
	}
}

func AwinsB(a Alternative, b Alternative, p Profile) (bool, bool) {
	count := make(map[Alternative]int)
	for _, i := range p {
		if isPref(a, b, i) {
			count[a] += 1
		} else if a != b {
			count[b] += 1
		} else {

		}
	}
	return count[a] > count[b], count[a] == count[b]
}
func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("ERROR CONDORCET WINNER")
	}
	if len(p) == 1 {
		return p[0], nil
	}
	map_winners := make(map[Alternative]int)
	for _, elem := range p[0] {
		//for _, x := range p[0][ind_elem+1:] { good idea but not enough time to do it (trinagular matrix)
		for _, x := range p[0] {
			bool_win, _ := AwinsB(elem, x, p)
			if bool_win {
				map_winners[elem] += 1
			}
		}
	}
	for item, _ := range map_winners {
		if map_winners[item] == len(p[0])-1 {
			return []Alternative{item}, nil
		}
	}
	return nil, nil
}
func CopelandSWF(p Profile) (Count, error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("ERROR COPELANDSWF")
	}
	map_winners := make(map[Alternative]int)
	for _, elem := range p[0] {
		for _, x := range p[0] {
			if x != elem {
				bool_win, bool_draw := AwinsB(elem, x, p)
				if bool_win {
					map_winners[elem] += 1
				} else if bool_draw {

				} else {
					map_winners[elem] -= 1
				}
			}
		}
	}
	return map_winners, nil
}
func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("ERROR CopelandSCF")

	}
	count, er := CopelandSWF(p)
	if er != nil {
		return nil, fmt.Errorf("ERROR CopelandSCF")
	}
	return maxCount(count), nil
}

func IsElemInList(a []Alternative, elem Alternative) bool {
	for _, i := range a {
		if i == elem {
			return true
		}
	}
	return false
}

func removeValue(slice []Alternative, value Alternative) []Alternative {
	var result []Alternative

	for _, v := range slice {
		if v != value {
			result = append(result, v)
		}
	}

	return result
}

var list_losers []Alternative

func STV_SWF(p Profile) (count Count, err error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("erreur STV_swf")
	}

	for {
		count := make(map[Alternative]int)
		for _, l := range p[0] {
			if !IsElemInList(list_losers, l) {
				count[l] = 0
			}
		}
		for _, i := range p {
			for _, j := range i {
				if !IsElemInList(list_losers, j) {
					count[j] += 1
					break
				}
			}
		}
		min := math.MaxInt64
		for key, val := range count {
			if len(count) == 1 {
				return count, nil
			}
			if len(p)%2 == 0 && count[key] >= (len(p)/2) {
				return count, nil
			}
			if len(p)%2 == 1 && count[key] > (len(p)/2) {
				return count, nil
			}
			if val < min {
				min = val
			}
		}
		var minKeys []Alternative
		for k, v := range count {
			if v == min {
				minKeys = append(minKeys, k)
			}
		}
		size := len(minKeys)
		for i := 1; i < size; i++ {
			elem_supp, _ := tiebreak_general(minKeys)
			minKeys = removeValue(minKeys, elem_supp)
		}
		list_losers = append(list_losers, minKeys[0])
	}
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	if len(p) == 0 {
		return nil, fmt.Errorf("erreur stv_scf")
	}
	count, er := STV_SWF(p)
	if er != nil {
		return nil, fmt.Errorf("erreur stv_scf")
	}
	max := math.MinInt64
	for _, val := range count {
		if max < val {
			max = val
		}
	}
	for key, val := range count {
		if max == val {
			bestAlts = append(bestAlts, key)
		}
	}
	return bestAlts, nil
}

//func ApprovalSWF(p Profile, thresholds []int) (count Count, err error)
//func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error)

// question par rapport tie break
// question par rapport thresh hold QST 3

// """
// var max int
// for item, _ := range map_winners {
// 	if max < map_winners[item] {
// 		max = map_winners[item]
// 	}
// }

// var answer []Alternative
// for item, _ := range map_winners {
// 	if max == map_winners[item] {
// 		answer = append(answer, item)
// 	}
// }
// """

//func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
//}
