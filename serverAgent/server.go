package td5AdnaneModule

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"reflect"
	"strconv"
	"sync"
	"time"

	rad "gitlab.utc.fr/merrazin/IA04_TD_PROJET"
	comsoc "gitlab.utc.fr/merrazin/IA04_TD_PROJET/comsoc"
)

type RestServerAgent struct {
	sync.Mutex
	id             string
	reqCount       int
	addr           string
	Ballots        []rad.Ballot
	BallotsCounter int
	Voters         []rad.Voter
}

func NewRestServerAgent(addr string) *RestServerAgent {
	return &RestServerAgent{id: addr, addr: addr}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

// we can add interface to decodeRequest
func (*RestServerAgent) decodeReqNew_ballot(r *http.Request) (req rad.New_ballot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}
func isRFC3339(str string) bool {
	_, err := time.Parse(time.RFC3339, str)
	return err == nil
}
func (rsa *RestServerAgent) createBallot(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()
	rsa.reqCount++

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req_New_ballot, err := rsa.decodeReqNew_ballot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête

	// vérifier que la requête est valide
	if reflect.TypeOf(req_New_ballot.Alts) != reflect.TypeOf(0) {
		w.WriteHeader(http.StatusNotImplemented)
		//msg := fmt.Sprintf("Unkonwn command '%s'", req.Operator)
		msg := fmt.Sprintf("Unkonwn command")
		w.Write([]byte(msg))
		return
	}
	// vérififer le format de la deadline
	if !isRFC3339(req_New_ballot.Deadline) {
		msg := fmt.Sprintf("Unkonwn command, problem with deadline")
		w.Write([]byte(msg))
		return
	}
	// vérifier que la deadline est dans le futur
	parsedTimeDeadline, err := time.Parse(time.RFC3339, req_New_ballot.Deadline)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid deadline (parsing RFC3339 error)")
		w.Write([]byte(msg))
		return
	}
	if time.Now().After(parsedTimeDeadline) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid deadline (deadline is in the past)")
		w.Write([]byte(msg))
		return
	}

	// vérifier que le nombre d'alternatives est cohérent, ça vérifie également le type de alts et tie-break
	if len(req_New_ballot.Tie_break) != req_New_ballot.Alts {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid number of alternatives or alts")
		w.Write([]byte(msg))
		return
	}

	// test if rule is valid
	rule_found := false
	list_of_rules := []string{"majority", "stv", "borda", "approval", "copeland", "condorcet"}
	for _, rule := range list_of_rules {
		if req_New_ballot.Rule == rule {
			rule_found = true
		}
	}
	if !rule_found {
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("Unkonwn rule")
		w.Write([]byte(msg))
		return
	}
	// ajout du résultat dans la réponse
	var resp rad.Response_New_ballot
	resp.Ballot_id = "scrutin" + strconv.Itoa(rsa.BallotsCounter)

	// ajout du ballot dans le serveur
	voterIDs := make(map[string]bool)
	for _, voter_id := range req_New_ballot.Voter_ids { // initialisation de la map
		voterIDs[voter_id] = false
	}
	rsa.Ballots = append(rsa.Ballots, rad.Ballot{Ballot_id: resp.Ballot_id, Rule: req_New_ballot.Rule, Deadline: req_New_ballot.Deadline, Voter_ids: voterIDs, Alts: req_New_ballot.Alts, Tie_break: req_New_ballot.Tie_break})
	rsa.BallotsCounter++

	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) decodeReqVote(r *http.Request) (req rad.Vote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////
func areElementsAllUnique(sl []int) bool {
	dict := make(map[int]bool)
	for _, elem := range sl {
		if dict[elem] {
			return false
		}
		dict[elem] = true
	}
	return true
}

func (rsa *RestServerAgent) addVote(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête
	req_New_vote, err := rsa.decodeReqVote(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// traitement de la requête
	// vérifier que la requête est valide

	// test if ballot exists and get the ballot
	ballotExists := false
	var CurrentBallot rad.Ballot
	for _, ball := range rsa.Ballots {
		if req_New_vote.Ballot_id == ball.Ballot_id {
			ballotExists = true
			CurrentBallot = ball
		}
	}
	if !ballotExists {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid ballot_id // ballot does not exist")
		w.Write([]byte(msg))
		return
	}
	// test if agent_id is valid
	value, exists := CurrentBallot.Voter_ids[req_New_vote.Agent_id]
	if !exists {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid agent_id")
		w.Write([]byte(msg))
		return
	}
	if value {
		w.WriteHeader(http.StatusForbidden)
		msg := fmt.Sprintf("vote déjà effectué")
		w.Write([]byte(msg))
		return
	}

	// test if agent_id is a string
	if reflect.TypeOf(req_New_vote.Agent_id) != reflect.TypeOf("") {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid agent_id")
		w.Write([]byte(msg))
		return
	}

	// test if the deadline has passed
	parsedTimeDeadline, err := time.Parse(time.RFC3339, CurrentBallot.Deadline)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid deadline (parsing RFC3339 error)")
		w.Write([]byte(msg))
		return
	}
	if time.Now().After(parsedTimeDeadline) {
		w.WriteHeader(http.StatusServiceUnavailable)
		msg := fmt.Sprintf("la deadline est depassee")
		w.Write([]byte(msg))
		return
	}
	// test if options are not empty while rule is approval or invalid options
	if CurrentBallot.Rule == "approval" && len(req_New_vote.Options) != 1 {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("field options is empty while rule is approval or invalid options")
		w.Write([]byte(msg))
		return
	}
	if CurrentBallot.Rule == "approval" && (req_New_vote.Options[0] > len(req_New_vote.Prefs)) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("while rule is approval, options value is invalid (options value is greater than the number of alternatives)")
		w.Write([]byte(msg))
		return
	}

	// test if preferences length is valid
	if CurrentBallot.Alts != len(req_New_vote.Prefs) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid preferences(length is not equal to the number of alternatives)")
		w.Write([]byte(msg))
		return
	}
	// test if elements are all unique
	if !areElementsAllUnique(req_New_vote.Prefs) {
		w.WriteHeader(http.StatusBadRequest)
		msg := fmt.Sprintf("invalid preferences (elements are not all unique)")
		w.Write([]byte(msg))
		return
	}
	// test if elements are all between 0 and Alts-1
	for _, pref := range req_New_vote.Prefs {
		bool_pref := false
		for _, tie_break := range CurrentBallot.Tie_break {
			if pref == tie_break {
				bool_pref = true
			}
		}
		if !bool_pref {
			w.WriteHeader(http.StatusBadRequest)
			msg := fmt.Sprintf("invalid preferences (elements are not all between 0 and Alts-1)")
			w.Write([]byte(msg))
			return
		}
	}

	// PROBLEM 501 not implemented isn't implemented StatusNotImplemented

	// update voter_ids map to true for the voter who voted (to prevent him from voting again)
	for voter_ids, _ := range CurrentBallot.Voter_ids {
		if req_New_vote.Agent_id == voter_ids {
			CurrentBallot.Voter_ids[voter_ids] = true
		}
	}
	// ajout des votes dans le serveur
	rsa.Voters = append(rsa.Voters, rad.Voter{Ballot_id: CurrentBallot.Ballot_id, Prefs: req_New_vote.Prefs, Options: req_New_vote.Options})
	w.WriteHeader(http.StatusOK)
	msg := fmt.Sprintf("L agent %v a voté.", req_New_vote.Agent_id)
	w.Write([]byte(msg))
}

func (rsa *RestServerAgent) decodeGetResult(r *http.Request) (req rad.Result, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestServerAgent) getResult(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	// décodage de la requête
	req_Result, err := rsa.decodeGetResult(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}
	// traitement de la requête
	// vérifier que la requête est valide

	// test if ballot exists
	ballotExists := false
	var CurrentBallot rad.Ballot
	for _, ball := range rsa.Ballots {
		if req_Result.Ballot_id == ball.Ballot_id {
			ballotExists = true
			CurrentBallot = ball
		}
	}
	if !ballotExists {
		w.WriteHeader(http.StatusNotFound)
		msg := fmt.Sprintf("invalid ballot_id // ballot does not exist")
		w.Write([]byte(msg))
		return
	}
	// test if a voter has not already voted
	for _, boolVoter := range CurrentBallot.Voter_ids {
		if !boolVoter {
			// test if it's too early to get the result
			parsedTimeDeadlineResult, err := time.Parse(time.RFC3339, CurrentBallot.Deadline)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				msg := fmt.Sprintf("invalid deadline (parsing RFC3339 error in result request)")
				w.Write([]byte(msg))
				return
			}
			if time.Now().Before(parsedTimeDeadlineResult) {
				w.WriteHeader(http.StatusTooEarly)
				msg := fmt.Sprintf("Too early")
				w.Write([]byte(msg))
				return
			}
		}
	}

	// partie vote TD
	// créantion du profile
	var profile_int [][]int
	var options []int
	for _, voter := range rsa.Voters {
		if voter.Ballot_id == CurrentBallot.Ballot_id {
			profile_int = append(profile_int, voter.Prefs)
			if CurrentBallot.Rule == "approval" {
				options = append(options, voter.Options[0])
			}
		}
	}
	// convert profile_int to profile
	var profile comsoc.Profile
	for _, prefs := range profile_int {
		var prefs_alternative []comsoc.Alternative
		for _, pref := range prefs {
			prefs_alternative = append(prefs_alternative, comsoc.Alternative(pref))
		}
		profile = append(profile, prefs_alternative)
	}

	switch CurrentBallot.Rule {
	case "majority":
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.MajoritySCF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}
		swfFunc := func(p comsoc.Profile) (comsoc.Count, error) {
			return comsoc.MajoritySWF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		ranking, err1 := comsoc.SWFFactory(swfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)
		winner, err2 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in Majority Functions")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		var Response_Result rad.Response_Result
		Response_Result.Winner = int(winner) // TO FIX  by adding tie-break!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		Response_Result.Ranking = comsoc.ConvertAltsToInt(ranking)
		serial, _ := json.Marshal(Response_Result)
		w.Write(serial)

	case "borda":
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.BordaSCF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}
		swfFunc := func(p comsoc.Profile) (comsoc.Count, error) {
			return comsoc.BordaSWF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		ranking, err1 := comsoc.SWFFactory(swfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)
		winner, err2 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in Borda Functions")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		var Response_Result rad.Response_Result
		Response_Result.Winner = int(winner)
		Response_Result.Ranking = comsoc.ConvertAltsToInt(ranking)
		serial, _ := json.Marshal(Response_Result)
		w.Write(serial)

	case "approval": //à implémenter car ça nécessite l'argument options que j'ai pas encore testé
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.ApprovalSCF(p, options) // Remplacez BordaSWF par votre fonction SWF réelle
		}
		swfFunc := func(p comsoc.Profile) (comsoc.Count, error) {
			return comsoc.ApprovalSWF(p, options) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		ranking, err1 := comsoc.SWFFactory(swfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)
		winner, err2 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in Approval Functions")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		var Response_Result rad.Response_Result
		Response_Result.Winner = int(winner)
		Response_Result.Ranking = comsoc.ConvertAltsToInt(ranking)
		serial, _ := json.Marshal(Response_Result)
		w.Write(serial)

	case "condorcet":
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.CondorcetWinner(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		winner, err1 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in CondorcetWinner")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		if winner == comsoc.Alternative(math.MinInt64) {
			msg := fmt.Sprintf("Il n'y a pas de gagnant de concorcet")
			w.Write([]byte(msg))
		} else {
			var Response_Result rad.Response_Result
			Response_Result.Winner = int(winner)
			serial, _ := json.Marshal(Response_Result)
			w.Write(serial)
		}

	case "copeland":
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.CopelandSCF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}
		swfFunc := func(p comsoc.Profile) (comsoc.Count, error) {
			return comsoc.CopelandSWF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		ranking, err1 := comsoc.SWFFactory(swfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)
		winner, err2 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in Copeland Functions")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		var Response_Result rad.Response_Result
		Response_Result.Winner = int(winner)
		Response_Result.Ranking = comsoc.ConvertAltsToInt(ranking)
		serial, _ := json.Marshal(Response_Result)
		w.Write(serial)

	case "stv":
		scfFunc := func(p comsoc.Profile) ([]comsoc.Alternative, error) {
			return comsoc.STV_SCF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}
		swfFunc := func(p comsoc.Profile) (comsoc.Count, error) {
			return comsoc.STV_SWF(p) // Remplacez BordaSWF par votre fonction SWF réelle
		}

		ranking, err1 := comsoc.SWFFactory(swfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)
		winner, err2 := comsoc.SCFFactory(scfFunc, comsoc.TieBreakFactory(comsoc.ConvertIntToAlts(CurrentBallot.Tie_break)))(profile)

		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusNotFound)
			msg := fmt.Sprintf("error in STV functions")
			w.Write([]byte(msg))
			return
		}
		w.WriteHeader(http.StatusOK)
		var Response_Result rad.Response_Result
		Response_Result.Winner = int(winner)
		Response_Result.Ranking = comsoc.ConvertAltsToInt(ranking)
		serial, _ := json.Marshal(Response_Result)
		w.Write(serial)

	default:
		w.WriteHeader(http.StatusNotImplemented)
		msg := fmt.Sprintf("This should not happen, because we tested the rule in createBallot")
		w.Write([]byte(msg))
		return
	}
}

func (rsa *RestServerAgent) doReqcount(w http.ResponseWriter, r *http.Request) {
	if !rsa.checkMethod("GET", w, r) {
		return
	}

	w.WriteHeader(http.StatusOK)
	rsa.Lock()
	defer rsa.Unlock()
	serial, _ := json.Marshal(rsa.reqCount)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/newBallot", rsa.createBallot)
	mux.HandleFunc("/vote", rsa.addVote)
	mux.HandleFunc("/result", rsa.getResult)
	mux.HandleFunc("/reqcount", rsa.doReqcount)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
