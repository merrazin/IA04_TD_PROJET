package td5AdnaneModule

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	rad "gitlab.utc.fr/merrazin/IA04_TD_PROJET"
)

type RestClientVoterAgent struct {
	Agent_id  string
	Ballot_id string
	Prefs     []int
	Options   []int
	Url       string
}

type RestClientBallotCreatorAgent struct {
	Ballot_id string // questionnable...
	Rule      string
	Deadline  string
	Voter_ids []string
	Alts      int
	Tie_break []int
	Url       string
}
type RestClientResultAgent struct {
	Ballot_id string
	Url       string
}

func NewRestClientBallotCreatorAgent(rule string, deadline string, voter_ids []string, alts int, tie_break []int, url string) *RestClientBallotCreatorAgent {
	return &RestClientBallotCreatorAgent{Rule: rule, Deadline: deadline, Voter_ids: voter_ids, Alts: alts, Tie_break: tie_break, Url: url}
}

func NewRestClientVoterAgent(agent_id string, ballot_id string, prefs []int, options []int, url string) *RestClientVoterAgent {
	return &RestClientVoterAgent{Agent_id: agent_id, Ballot_id: ballot_id, Prefs: prefs, Options: options, Url: url}
}

func NewRestClientResultAgent(ballot_id string, url string) *RestClientResultAgent {
	return &RestClientResultAgent{Ballot_id: ballot_id, Url: url}
}

func (rcBCa *RestClientBallotCreatorAgent) TreatResponseBallotAgent(r *http.Response) rad.Response_New_ballot {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response_New_ballot
	json.Unmarshal(buf.Bytes(), &resp)
	return resp
}

var mu sync.Mutex

func (rcBCa *RestClientBallotCreatorAgent) CreateBallot() (rad.Response_New_ballot, error) {
	req := rad.New_ballot{
		Rule:      rcBCa.Rule,
		Deadline:  rcBCa.Deadline,
		Voter_ids: rcBCa.Voter_ids,
		Alts:      rcBCa.Alts,
		Tie_break: rcBCa.Tie_break,
	}
	// sérialisation de la requête
	url := rcBCa.Url + "/newBallot"
	data, _ := json.Marshal(req)
	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return rad.Response_New_ballot{Ballot_id: ""}, err
	}
	// traitement de la réponse
	if resp.StatusCode != http.StatusCreated {
		err = fmt.Errorf(("[%d] %s"), resp.StatusCode, resp.Status)
	}
	res := rcBCa.TreatResponseBallotAgent(resp)
	rcBCa.Ballot_id = res.Ballot_id
	return res, nil
}

func (rcVA *RestClientVoterAgent) Vote() error {
	req := rad.Vote{
		Agent_id:  rcVA.Agent_id,
		Ballot_id: rcVA.Ballot_id,
		Prefs:     rcVA.Prefs,
		Options:   rcVA.Options,
	}
	// sérialisation de la requête
	url := rcVA.Url + "/vote"
	data, _ := json.Marshal(req)
	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	// traitement de la réponse
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf(("[%d] %s"), resp.StatusCode, resp.Status)
	}
	return nil
}

func (rcRA *RestClientResultAgent) TreatResponseResultAgent(r *http.Response) rad.Response_Result {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.Response_Result
	json.Unmarshal(buf.Bytes(), &resp)
	return resp
}

func (rcRA *RestClientResultAgent) Result() (rad.Response_Result, error) {
	req := rad.Result{
		Ballot_id: rcRA.Ballot_id,
	}
	// sérialisation de la requête
	url := rcRA.Url + "/result"
	data, _ := json.Marshal(req)
	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return rad.Response_Result{Winner: -1, Ranking: []int{}}, err
	}
	// traitement de la réponse
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf(("[%d] %s"), resp.StatusCode, resp.Status)
		return rad.Response_Result{Winner: -1, Ranking: []int{}}, err
	}
	res := rcRA.TreatResponseResultAgent(resp)
	return res, nil
}

func agentIDExists(agentID string, voterIDs []string) bool {
	for _, id := range voterIDs {
		if id == agentID {
			return true
		}
	}
	return false
}

func (rcBCa *RestClientBallotCreatorAgent) Start(voters *[]RestClientVoterAgent, result *RestClientResultAgent) {
	mu.Lock()
	rcBCa.CreateBallot()
	if rcBCa.Ballot_id == "" {
		mu.Unlock()
		return
	}

	var wg sync.WaitGroup

	for _, voter := range *voters {
		if agentIDExists(voter.Agent_id, rcBCa.Voter_ids) {
			voter.Ballot_id = rcBCa.Ballot_id
			wg.Add(1)
			go func(voter RestClientVoterAgent) {
				defer wg.Done()
				voter.Vote()
			}(voter)
			time.Sleep(30 * time.Millisecond)
		}
	}

	wg.Wait()

	result.Ballot_id = rcBCa.Ballot_id
	res, err := result.Result()
	if err != nil {
		log.Fatal(rcBCa.Ballot_id, "error", err.Error())
	} else {
		log.Printf("Résultat du %s avec la méthode %s :\n", rcBCa.Ballot_id, rcBCa.Rule)
		log.Printf("	- Avec %d alternatives\n", rcBCa.Alts)
		log.Printf("	- Winner: %d\n", res.Winner)
		log.Printf("	- Ranking: %v\n\n\n", res.Ranking)
	}
	mu.Unlock()
}
